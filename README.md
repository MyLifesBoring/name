# BirdieMart online Supermarket shopping system


Admin is the administrator at the store that keeps track of the activities in store. 
These activities include keeping track of products, customers that are present in the store, the bill history and bill summary of the customers that are in the store.

Customer is the user that shops at the store. 
The bill history is also visible for the customers who are registered on the system. 
Once bills are paid the user is allowed to logout.

You can login and shop as a customer and track the activity as a admin. 
